package com.example.manuel.metricasapp;

import android.support.annotation.Nullable;

import java.util.Vector;

public class Automata {
    private String letras = "abcedfghijklmnñopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private String espacio = " ";
    private String salidas = "\n\t";
    private String operadoresS = "+-*/:=><,";
    private String numeros = "1234567890";
    private int def=0;
    private int iF=0;
    private int While=0;
    private int parentesis=0;
    private int suma=0;
    private int resta=0;
    private int division=0;
    private int multiplicacion=0;
    private int igual=0;
    private int mayor=0;
    private int menor=0;
    private int coma=0;
    private int dosP=0;
    Vector <String> tokens = new Vector<String>();
    Vector <String> variablesVec = new Vector<String>();
    Vector <Double> variablesNum = new Vector<Double>();

    private String [][] automataDef = {
            {"q0","d","q1"},
            {"q1","e","q2"},
            {"q2","f","q3"},
            {"q3","espacio","qf"},
    };//regresa 1

    private String [][] automataReturn = {
            {"q0","r","q1"},
            {"q1","e","q2"},
            {"q2","t","q3"},
            {"q3","u","q4"},
            {"q4","r","q5"},
            {"q5","n","q6"},
            {"q6","espacio","qf"},
    };//regresa 1

    private String [][] automataIf = {
            {"q0","i","q1"},
            {"q1","f","q2"},
            {"q2","espacio","qf"},
            {"q2","(","qf"},
    };//regresa 1

    private String [][] automataWhile = {
            {"q0","w","q1"},
            {"q1","h","q2"},
            {"q2","i","q3"},
            {"q3","l","q4"},
            {"q4","e","q5"},
            {"q5","espacio","qf"},
            {"q5","(","qf"},
    };//regresa 1

    /*private String [][] nombreFuncion = {
            {"q0","letras","q1"},
            {"q1","letras","q1"},
            {"q1","numeros","q1"},
            {"q1","espacio","qf"},
            {"q1","(","qf"},
    };//regresa 1*/

    private String [][] parentesisFuncion = {
            {"q0", "(", "qf"},
            {"q0", ")", "qf"},
    };
    //no regresa

    private String [][] variables = {
            {"q0","letras","q1"},
            {"q1","letras","q1"},
            {"q1","numeros","q1"},
            {"q1","salidas","qf"},
            {"q1","espacio","qf"},
            {"q1","operadores","qf"},
            {"q1","(","qf"},
            {"q1",")","qf"},
    };//regresa 1

    private String [][] operNumeros = {
            {"q0","numeros","q1"},
            {"q1","numeros","q1"},
            {"q1","salidas","qf"},
            {"q1","espacio","qf"},
            {"q1","operadores","qf"},
            {"q1","(","qf"},
            {"q1",")","qf"},
    };//regresa 1

    private String [][] operadores = {
            {"q0","operadores","qf"}
    };//no regresa

    private String [][] ignore = {
            {"q0","salidas","qf"},
            {"q0","espacio","qf"}
    };//no regresa

    public String initMetricas(String codigo)
    {

        int apuntador = 0;
        int topeAnt;
        String token;
        String [] automatas = new String[]{
                "def",
                "automataReturn",
                "automataIf",
                "automataWhile",
                "variables",
                "operNumeros",
                "parentesis",
                "operadores",
                "ignore"
        };

        while (apuntador < codigo.length())
        {
            int j = 0;//indice que recorre los automatas
            int topeAux;
            while (j < automatas.length)
            {
                topeAux = this.valueAutomata(codigo,apuntador,automatas[j]);
                if(topeAux > 0)
                {
                    topeAnt = apuntador;
                    apuntador = topeAux;
                    token = codigo.substring(topeAnt,apuntador);
                    tokens.add(token);
                    if(automatas[j].equals("def") || token.equals("def"))
                    {
                        this.def++;
                    }else if(automatas[j].equals("automataIf") || token.equals("if"))
                    {
                        this.iF++;
                    }else if (automatas[j].equals("parentesis") || token.equals("(") || token.equals(")"))
                    {
                        this.parentesis++;
                    }else if(automatas[j].equals("operadores"))
                    {
                        if(token.equals("+"))
                        {
                            this.suma++;
                        }else if(token.equals("-"))
                        {
                            this.resta++;
                        }else if (token.equals("/"))
                        {
                            this.division++;
                        }else if (token.equals("*"))
                        {
                            this.multiplicacion++;
                        }else if (token.equals("="))
                        {
                            this.igual++;
                        }else if (token.equals(">"))
                        {
                            this.mayor++;
                        }else if (token.equals("<"))
                        {
                            this.menor++;
                        }else if (token.equals(","))
                        {
                            this.coma++;
                        }else if(token.equals(":"))
                        {
                            this.dosP++;
                        }
                    }else if(automatas[j].equals("variables") || automatas[j].equals("operNumeros"))
                    {
                        if(!variablesVec.contains(token))
                        {
                            this.variablesVec.add(token);
                            this.variablesNum.add((double) 1);
                        }else {
                            for (int i = 0; i<variablesVec.size();i++)
                            {
                                if(variablesVec.get(i).equals(token))
                                {
                                    Double val = this.variablesNum.get(i);
                                    val = val + 1;
                                    this.variablesNum.set(i,val);
                                }
                            }
                        }
                    }

                    //return apuntador;

                    break;
                }else {
                    j++;
                }
            }
            if(j == automatas.length)
            {
                return null;
            }
        }
        return String.valueOf(this.variablesNum.get(0));
    }

    private int valueAutomata(String codigo,int apuntado,String AutomataTipo){

        int j = apuntado;//recorre el codigo
        int indiceEstado = 0; //recorre los estados del automata
        String estadoActual = "q0"; //estado actual
        String [][] automata = this.automataTipo(AutomataTipo); //trae automata a utilizar
        boolean otro = false;
        if (automata == null)
        {
            return 0;
        }
        while (indiceEstado < automata.length && j<codigo.length())
        {
            String estadoAutomata = automata[indiceEstado][0];
            if(estadoActual.equals("qf"))
            {
                if(AutomataTipo.equals("operadores") || AutomataTipo.equals("ignore") || AutomataTipo.equals("parentesis")) {
                    return j;
                }
                return j-1;
            }else{
                if(estadoActual.equals(estadoAutomata)) {
                    if (this.contiene(String.valueOf(codigo.charAt(j)),automata[indiceEstado][1]))
                    {
                        estadoActual = automata[indiceEstado][2];
                        indiceEstado = 0;//0
                        j++;
                    }else
                    {
                        indiceEstado++;
                    }
                }else
                {
                    indiceEstado++;
                }
            }
        }

        if(estadoActual.equals("qf"))
        {
            if(AutomataTipo.equals("operadores") || AutomataTipo.equals("ignore") || AutomataTipo.equals("parentesis")) {
                return j;
            }
            return j-1;
        }

        return 0;
    }

    @Nullable
    private String[][] automataTipo(String tipo)
    {
        if(tipo.equals("def"))
            return this.automataDef;
        else if (tipo.equals("automataIf"))
            return this.automataIf;
        else if (tipo.equals("automataWhile"))
            return this.automataWhile;
        else if (tipo.equals("operNumeros"))
            return this.operNumeros;
        else if (tipo.equals("parentesis"))
            return this.parentesisFuncion;
        else if(tipo.equals("variables"))
            return this.variables;
        else if (tipo.equals("operadores"))
            return this.operadores;
        else if (tipo.equals("ignore"))
            return this.ignore;
        else if (tipo.equals("automataReturn"))
            return this.automataReturn;

        return null;
    }

    private boolean contiene(String cad,String tipo)
    {
        if(tipo.equals("letras")) {
            if (this.letras.contains(cad))
                return true;
        }else if(tipo.equals("salidas")) {
            if (this.salidas.contains(cad))
                return true;
        }else if(tipo.equals("operadores")) {
            if (this.operadoresS.contains(cad))
                return true;
        }else if(tipo.equals("d")) {
            if ("d".contains(cad))
                return true;
        }else if(tipo.equals("e")) {
            if ("e".contains(cad))
                return true;
        }else if(tipo.equals("f")) {
            if ("f".contains(cad))
                return true;
        }else if(tipo.equals("espacio")) {
            if (this.espacio.contains(cad))
                return true;
        }else if(tipo.equals("(")) {
            if ("(".contains(cad))
                return true;
        }else if(tipo.equals(")")) {
            if (")".contains(cad))
                return true;
        }else if(tipo.equals("numeros")) {
            if (this.numeros.contains(cad))
                return true;
        }else if(tipo.equals("i")) {
            if ("i".contains(cad))
                return true;
        }else if(tipo.equals("f")) {
            if ("f".contains(cad))
                return true;
        }else if (tipo.equals("w")) {
            if ("w".contains(cad))
                return true;
        }
        else if (tipo.equals("h")) {
            if ("h".contains(cad))
                return true;
        }else if (tipo.equals("l")) {
            if ("l".contains(cad))
                return true;
        }else if (tipo.equals("r")) {
            if ("r".contains(cad))
                return true;
        }else if (tipo.equals("t")) {
            if ("t".contains(cad))
                return true;
        }else if (tipo.equals("u")) {
            if ("u".contains(cad))
                return true;
        }else if (tipo.equals("n")) {
            if ("n".contains(cad))
                return true;
        }

        return false;
    }

    public int getDef()
    {
        return this.def;
    }

    public int getiF()
    {
        return this.iF;
    }

    public int getWhile()
    {
        return this.While;
    }

    public int getParentesis()
    {
        return this.parentesis;
    }

    public int getSuma()
    {
        return this.suma;
    }

    public int getResta()
    {
        return this.resta;
    }

    public int getDivision()
    {
        return this.division;
    }

    public int getMultiplicacion()
    {
        return this.multiplicacion;
    }

    public int getIgual(){
        return this.igual;
    }

    public int getMayor(){
        return this.mayor;
    }

    public int getMenor()
    {
        return this.menor;
    }

    public int getComa()
    {
        return this.coma;
    }

    public int getDosP()
    {
        return this.dosP;
    }

    public Vector<Double> getVariablesNum() {
        return variablesNum;
    }

    public Vector<String> getVariablesVec() {
        return variablesVec;
    }
}
