package com.example.manuel.metricasapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MetricasDB extends AppCompatActivity {

    private Button regresar;
    private Button buscar;
    private EditText nombre;
    private EditText resultado;
    private Button verTodo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metricas_db);

        this.regresar = (Button) findViewById(R.id.btn_regresarMain);
        this.buscar = (Button) findViewById(R.id.buscar_btn);
        this.nombre = (EditText) findViewById(R.id.nombreArch_txt);
        this.resultado = (EditText) findViewById(R.id.etx_resultado);
        this.verTodo = (Button) findViewById(R.id.btn_viewAll);

        this.regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        this.nombre.setText(getIntent().getStringExtra("nombre"));


        this.buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getApplicationContext(),"administracion",null,1);
                SQLiteDatabase Base = admin.getWritableDatabase();

                String name = nombre.getText().toString();

                if (!name.isEmpty())
                {
                    Cursor fila = Base.rawQuery("SELECT nombre,n1,N_1,n2,N_2,N,n_,V,D,L,E,T,B FROM Metricas WHERE nombre LIKE '"+name+"'",null);
                    if (fila.moveToFirst())
                    {
                        String metricasResult = "n1  = " + fila.getString( 1) + "\nN1  = " + fila.getString( 2) +" \nn2  = "
                                + fila.getString( 3) + "\nN2  = " + fila.getString( 4) + "\nN   = " + fila.getString( 5) + "\nn   ="
                                + fila.getString( 6) + "\nV   = " + fila.getString( 7) + "\nD   = " + fila.getString( 8) + "\nL   = "
                                + fila.getString( 9) + "\nE   = " + fila.getString( 10) + "\nT   = " + fila.getString( 11) + "\nB   = " + fila.getString( 12);
                        resultado.setText(metricasResult);
                    }else {
                        Toast.makeText(getApplicationContext(),"No existe guardado resultados de ese archivo en la base de datos",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"Debes Ingersar Nombre",Toast.LENGTH_SHORT).show();
                }
                Base.close();
            }
        });

        this.verTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MetricasDB.this,AllDB.class);
                startActivity(intent);
            }
        });

    }
}
