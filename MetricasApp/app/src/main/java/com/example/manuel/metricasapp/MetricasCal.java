package com.example.manuel.metricasapp;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MetricasCal extends AppCompatActivity {

    private  EditText result;
    private Button guardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metricas_cal);

        result = (EditText) findViewById(R.id.result_txt);
        guardar = (Button) findViewById(R.id.button_base);

        final int n1 = getIntent().getIntExtra("n1",0);
        final int N1 = getIntent().getIntExtra("N1",0);
        final int n2 = getIntent().getIntExtra("n2",0);
        final int N2 = getIntent().getIntExtra("N2",0);
        final int N = getIntent().getIntExtra("N",0);
        final int n = getIntent().getIntExtra("n",0);
        final Double V = getIntent().getDoubleExtra("V",0);
        final Double D = getIntent().getDoubleExtra("D",0);
        final Double L = getIntent().getDoubleExtra("L",0);
        final Double E = getIntent().getDoubleExtra("E",0);
        final Double T = getIntent().getDoubleExtra("T",0);
        final Double B = getIntent().getDoubleExtra("B",0);
        final String name = getIntent().getStringExtra("nombre");


        String reporte =
                "n1  = " + n1 + "\nN1  = " + N1 +" \nn2  = "
                        + n2 + "\nN2  = " + N2 + "\nN   = " + N + "\nn   ="
                        + n + "\nV   = " + V + "\nD   = " + D + "\nL   = "
                        + L + "\nE   = " + E + "\nT   = " + T + "\nB   = " + B;

        this.result.setText(reporte);


       this.guardar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getApplicationContext(),"administracion",null,1);
               SQLiteDatabase base = admin.getWritableDatabase();

               ContentValues registro = new ContentValues();
               registro.put("nombre",name);
               registro.put("n1",n1);
               registro.put("N_1",N1);
               registro.put("n2",n2);
               registro.put("N_2",N2);
               registro.put("N",N);
               registro.put("n_",n);
               registro.put("V",V);
               registro.put("D",D);
               registro.put("L",L);
               registro.put("E",E);
               registro.put("T",T);
               registro.put("B",B);
               base.insert("Metricas",null,registro);
               base.close();

               Toast.makeText(getApplicationContext(),"Guardado en la base",Toast.LENGTH_SHORT).show();
               finish();
           }
       });
    }





}
