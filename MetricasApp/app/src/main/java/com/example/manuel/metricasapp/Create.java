package com.example.manuel.metricasapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Vector;

public class Create extends AppCompatActivity {

    private Button calculate;
    private Button ret;
    private EditText code;
    private EditText name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        ret = (Button) findViewById(R.id.btn_regresar);
        calculate = (Button) findViewById(R.id.btn_caulculate);
        code = (EditText) findViewById(R.id.txt_code);
        name = (EditText) findViewById(R.id.txt_name);

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.length()>1) {
                    String[] archivos = fileList();
                    String nameArch = name.getText().toString() + ".txt";
                    if (!archivoExise(archivos, nameArch)) {
                        try {
                            OutputStreamWriter archivo = new OutputStreamWriter(openFileOutput(nameArch, Activity.MODE_PRIVATE));
                            archivo.write(code.getText().toString());
                            archivo.flush();
                            archivo.close();
                            //Toast.makeText(getApplicationContext(),code.getText().toString(),Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(), "Guardado con exito.\nPor favor pulse guardar en la base de datos", Toast.LENGTH_SHORT).show();

                            Automata aut = new Automata();
                            String a = aut.initMetricas(code.getText().toString());

                            int d = aut.getParentesis();
                            //Toast.makeText(getApplicationContext(),"El resultado es "+ a +" numero de parentesis es "+d,Toast.LENGTH_SHORT).show();
                            calcularMetricas(aut,nameArch);
                        } catch (IOException e) {

                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Ya existe un archivo con este nombre", Toast.LENGTH_SHORT).show();
                    }
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(),"Ingrea el nombre del archivo",Toast.LENGTH_LONG).show();
                }
            }
        });

        ret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean archivoExise(String  archivos[],String name)
    {
        for (int i = 0; i < archivos.length;i++)
            if(name.equals(archivos[i]))
                return true;

        return false;
    }

    private void calcularMetricas(Automata automata,String name)
    {
        // numero de operadores
        int n1 = nomOperadores(automata);
        //Toast.makeText(getApplicationContext(),"Hay "+n1+" numero operadores",Toast.LENGTH_LONG).show();

        //numero total de operadores
        int N1 = totalOperadores(automata);
        //Toast.makeText(getApplicationContext(),"Hay "+N1+" total operadores",Toast.LENGTH_LONG).show();

        //numero de operandos
        int n2 = nomOperandos(automata);
        //Toast.makeText(getApplicationContext(),"Hay "+n2+" numero operandos",Toast.LENGTH_LONG).show();

        //numero total de operandos
        int N2 = totalOperandos(automata);
        //Toast.makeText(getApplicationContext(),"Hay "+N2+" total operandos",Toast.LENGTH_LONG).show();

        //longitud del programa
        int N = N1 + N2;

        //Vocabulario de P
        int n = n1 + n2;

        //Volumen
        Double V = N * (Math.log(n)/Math.log(2));

        //Dificultad
        Double D = (Double.valueOf(n1)/2)*(Double.valueOf(N2)/Double.valueOf(n2));

        //Nivel
        Double L = 1/D;

        //Esfuerzo
        Double E = D*V;

        //Tiempo
        Double T = E/18;

        //Bugs
        Double B = Math.pow(T,2/3)/3000;

        Intent intent = new Intent(Create.this,MetricasCal.class);
        intent.putExtra("nombre",name);
        intent.putExtra("n1",n1);
        intent.putExtra("N1",N1);
        intent.putExtra("n2",n2);
        intent.putExtra("N2",N2);
        intent.putExtra("N",N);
        intent.putExtra("n",n);
        intent.putExtra("V",V);
        intent.putExtra("D",D);
        intent.putExtra("L",L);
        intent.putExtra("E",E);
        intent.putExtra("T",T);
        intent.putExtra("B",B);
        startActivity(intent);
    }

    private int nomOperadores(Automata automata)
    {
        int n = 0;

        if (automata.getSuma()>0)
            n++;
        if (automata.getResta()>0)
            n++;
        if (automata.getParentesis()>0)
            n++;
        if (automata.getMultiplicacion()>0)
            n++;
        if (automata.getMenor()>0)
            n++;
        if (automata.getMayor()>0)
            n++;
        if (automata.getIgual()>0)
            n++;
        if (automata.getiF()>0)
            n++;
        if (automata.getWhile()>0)
            n++;
        if (automata.getDosP()>0)
            n++;
        if (automata.getDivision()>0)
            n++;
        if (automata.getDef()>0)
            n++;
        if (automata.getComa()>0)
            n++;

        return n;

    }

    private int totalOperadores(Automata automata)
    {
        int n = automata.getSuma()+automata.getResta()+(automata.getParentesis()/2)
                +automata.getMultiplicacion()+automata.getMenor()+automata.getMayor()
                +automata.getIgual()+automata.getiF()+automata.getWhile()+automata.getDosP()+automata.getDivision()
                +automata.getDef()+automata.getComa();
        return n;
    }

    private int nomOperandos(Automata automata)
    {
        Vector <String> operandos = automata.getVariablesVec();
        int n = operandos.size();
        return n;
    }

    private int totalOperandos(Automata automata)
    {
        Double n = 0.0;
        Vector <Double> operandos = automata.getVariablesNum();
        for (int i = 0; i < operandos.size();i++)
            n = n + operandos.get(i);

        return n.intValue();
    }
}
 /*try {
                        //se debe guardar
                        Toast.makeText(getApplicationContext(), "Guardado Exitoramente", Toast.LENGTH_SHORT).show();
                        InputStreamReader archivo = new InputStreamReader(openFileInput(nameArch));
                        BufferedReader br = new BufferedReader(archivo);
                        String linea = br.readLine();
                        String CodigoCompleto = "";

                        while (linea != null) {
                            CodigoCompleto = CodigoCompleto + linea + "\n";
                            linea = br.readLine();
                        }
                        br.close();
                        archivo.close();
                    }catch (IOException e)
                    {

                    }*/
