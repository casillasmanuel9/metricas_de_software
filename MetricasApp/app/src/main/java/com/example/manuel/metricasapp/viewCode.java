package com.example.manuel.metricasapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Vector;

public class viewCode extends AppCompatActivity {

    private ListView codigos;
    private Button regresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_code);

        this.codigos = (ListView) findViewById(R.id.list_codes);
        this.regresar = (Button) findViewById(R.id.btn_regresar);
        final Vector <String> archivos2 = new Vector<String>();

        String[] archivos = fileList();
        for(int i=0;i<archivos.length;i++)
            if(!archivos[i].equals("instant-run") && !archivos[i].equals(".txt"))
                archivos2.add(archivos[i]);

        ArrayAdapter <String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,archivos2);
        codigos.setAdapter(adapter);

        codigos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(viewCode.this,viewCodeTxt.class);
                intent.putExtra("name",codigos.getItemAtPosition(position).toString());
                startActivity(intent);
            }
        });

        this.regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
