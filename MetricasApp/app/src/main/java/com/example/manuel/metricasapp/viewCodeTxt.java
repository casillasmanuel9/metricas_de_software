package com.example.manuel.metricasapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class viewCodeTxt extends AppCompatActivity {

    private EditText code;
    private Button ret;
    private Button view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_code_txt);


        //Toast.makeText(getApplicationContext(),getIntent().getStringExtra("name"),Toast.LENGTH_LONG).show();

        this.ret = (Button) findViewById(R.id.btn_regresar);
        this.code = (EditText) findViewById(R.id.txt_code);
        this.view = (Button) findViewById(R.id.btn_verMet);

        final String name = getIntent().getStringExtra("name");
        try {
            InputStreamReader archivo = new InputStreamReader(openFileInput(name));
            BufferedReader br = new BufferedReader(archivo);
            String linea = br.readLine();
            String codigoCompleto = "";

            while (linea != null) {
                codigoCompleto = codigoCompleto + linea + "\n";
                linea = br.readLine();
            }
            br.close();
            archivo.close();
            this.code.setText(codigoCompleto);
        }catch (IOException e)
        {

        }

        this.ret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Pulse Buscar",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(viewCodeTxt.this,MetricasDB.class);
                intent.putExtra("nombre",name);
                startActivity(intent);
            }
        });
    }
}
