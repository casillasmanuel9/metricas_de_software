package com.example.manuel.metricasapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button create;
    private Button viewCode;
    private Button exit;
    private Button viewDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.create = (Button) findViewById(R.id.btn_create);
        this.viewCode = (Button) findViewById(R.id.btb_viewCode);
        this.exit = (Button) findViewById(R.id.btn_exit);
        this.viewDB = (Button) findViewById(R.id.btn_viewDB);

        this.create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Create.class);
                startActivity(intent);
            }
        });

        this.viewCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,viewCode.class);
                startActivity(intent);
            }
        });

        this.viewDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,MetricasDB.class);
                startActivity(intent);
            }
        });

        this.exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /*Automata aut = new Automata();
        aut.setEstado("MEXICO");

        Toast.makeText(getApplicationContext(),aut.getEstado(),Toast.LENGTH_LONG).show();*/

    }
}
