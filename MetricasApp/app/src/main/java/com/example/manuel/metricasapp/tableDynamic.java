package com.example.manuel.metricasapp;

import android.content.Context;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class tableDynamic {
    private TableLayout tableLayout;
    private Context context;
    private String [] header;
    private ArrayList<String []> data;
    private TableRow tableRow;
    private TextView textCell;
    private int indexC;
    private int indexR;


    public tableDynamic(TableLayout tableLayout, Context context) {
        this.tableLayout = tableLayout;
        this.context = context;
    }

    public void addHeader(String[] header)
    {
        this.header=header;
    }
    public void addHeader(ArrayList<String[]> data)
    {
        this.data=data;
    }

    private void newRow()
    {
        this.tableRow = new TableRow(context);
    }

    private void newCell()
    {
        textCell = new TextView(context);
        textCell.setGravity(Gravity.CENTER);
        textCell.setTextSize(25);
    }

    private void createHeader()
    {
        indexC = 0;
        newRow();
        while (indexC< header.length)
        {
            newCell();
            textCell.setText(header[indexC++]);
            tableRow.addView(textCell);

        }
        this.tableLayout.addView(tableRow);
    }


}
